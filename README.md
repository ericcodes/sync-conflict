# sync-conflict

A script for finding sync-conflict files using the syncthing API

## configuration

Create a file called `~/.config/sync-conflict.json` that contains

```json
{
  "syncthing-url": "http://localhost:8384/",
  "api-key": "..."
}
```

Your API Key can be found under `Actions -> Settings -> API Key`
