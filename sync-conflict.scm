#!/usr/bin/env -S guile -s
!#
(define-module (sync-conflict)
  #:use-module (ice-9 ftw)
  #:use-module (rnrs bytevectors)
  #:use-module (sjson utils)
  #:use-module (sjson)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-9)
  #:use-module (web client)
  #:use-module (web http)
  #:use-module (web response)
  #:export (sync-conflict-fold main))

(define (info msg)
  (format (current-error-port) "INFO: ~a" msg))

(define (config->folders config)
  "Extract the folder list from the config jsobj"
  (let* ((folders (jsobj-ref config "folders")))
    (map (cut jsobj-ref <> "path") folders)))

(define (sync-conflict? path)
  (string-contains path ".sync-conflict-"))

(define (io-syncthing-config base-uri api-key)
  "Fetch the config jsobj from the syncthing API

The resulting sjson is a super-set of the following structure:

folders:
  - path: string()
"
  (define-values (resp body)
    (http-get (string-append base-uri "rest/config")
              #:headers `((Authorization . ,(string-append "Bearer " api-key)))
              #:decode-body? #t))
  (read-json (open-input-string (utf8->string body))))

(define (io-fold-files path kons init)
  "Walk the path to find any syncconflict files"
  (define enter? (const #t))
  (define (leaf path stat result) (kons path result))
  (define (down path stat result) result)
  (define (up path stat result) result)
  (define (skip path stat result) result)
  (define (error path stat errno result) result)
  (file-system-fold enter? leaf down up skip error init path))

(define (sync-conflict-fold fetch-config
                            fold-files
                            kons
                            knil)
  "Calls FETCH-CONFIG and calls FOLD-FILES for each folder listed in the config.

(FETCH-CONFIG) returns a `sjson` value that is at least the following structure:

```
folders:
  - path: \"...\"
```

(FOLD-FILES START-PATH PROC INIT) traverses PATH and calls (PROC PATH RESULT) for each files in START-PATH

SYNC-CONFLICT-FOLD calls (KONS PATH RESULT) for each sync-conflict file found
"
  ;; fetch folders list from the syncthing config endpoint
  (define folders (config->folders (fetch-config)))

  ;; kons a given path if it is a sync-conflict file
  (define (kons-paths path result)
    (if (sync-conflict? path)
        (kons path result)
        result))

  ;; fold each folder using kons-paths
  (define (kons-folders folder result)
    (info (format #f "looking in ~s~%" folder))
    (fold-files folder kons-paths result))

  (fold kons-folders knil folders))

(define (main)
  (define cfg-filename (string-append (getenv "HOME") "/.config/sync-conflict.json"))
  (unless (file-exists? cfg-filename)
    (error (format #f "Config file ~s missing" cfg-filename)))

  (define cfg (call-with-input-file cfg-filename read-json))

  (define syncthing-url (jsobj-ref cfg "syncthing-url"))
  (unless syncthing-url
    (error (format #f "~s config field missing" "syncthing-url")))

  (define api-key (jsobj-ref cfg "api-key"))
  (unless api-key
    (error (format #f "~s config field missing" "api-key")))

  (sync-conflict-fold (cut io-syncthing-config syncthing-url api-key)
                      io-fold-files
                      (lambda (path result)
                        (display path)
                        (newline)
                        #f)
                      #f))

(main)
